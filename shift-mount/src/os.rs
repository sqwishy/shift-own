use std::os::unix::io::AsRawFd;
use std::os::unix::io::RawFd;
use std::path::{Path, PathBuf};

use libc::AT_FDCWD;
use nix::{errno::Errno, fcntl::OFlag, sys::stat::Mode};

static NEW_DIR_MODE: Mode = Mode::S_IRWXU;

/// RAII for file descriptors...
#[derive(Debug)]
pub enum Fd {
    Owned(RawFd),
    Cwd,
}

impl AsRawFd for Fd {
    fn as_raw_fd(&self) -> RawFd {
        match self {
            Fd::Owned(raw) => *raw,
            Fd::Cwd => AT_FDCWD,
        }
    }
}

impl Drop for Fd {
    fn drop(&mut self) {
        if let Fd::Owned(raw) = self {
            let _ = nix::unistd::close(*raw);
        }
    }
}

pub trait RawFdExt: AsRawFd {}

pub trait IsErrNo {
    fn is_file_exists(&self) -> bool {
        self.is_errno(Errno::EEXIST)
    }

    fn is_not_found(&self) -> bool {
        self.is_errno(Errno::ENOENT)
    }

    fn is_errno(&self, errno: Errno) -> bool;
}

impl IsErrNo for nix::Error {
    fn is_errno(&self, errno: Errno) -> bool {
        self.as_errno() == Some(errno)
    }
}

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("open {}", path.display())]
    Open { path: PathBuf, source: nix::Error },
    #[error("mkdir {}", path.display())]
    Mkdir { path: PathBuf, source: nix::Error },
}

impl IsErrNo for Error {
    fn is_errno(&self, errno: Errno) -> bool {
        match self {
            Error::Open { source, .. } => source.is_errno(errno),
            Error::Mkdir { source, .. } => source.is_errno(errno),
        }
    }
}

type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug)]
pub struct Dir(Fd);

impl From<Fd> for Dir {
    fn from(o: Fd) -> Self {
        Dir(o)
    }
}

impl std::ops::Deref for Dir {
    type Target = Fd;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Dir {
    pub fn cwd() -> Self {
        Dir(Fd::Cwd)
    }

    pub fn mkdirat<P: AsRef<Path>>(&self, path: P) -> Result<()> {
        let path = path.as_ref();
        nix::sys::stat::mkdirat(self.as_raw_fd(), path, NEW_DIR_MODE).map_err(|source| {
            Error::Mkdir {
                path: path.into(),
                source,
            }
        })
    }

    pub fn opendirat<P: AsRef<Path>>(&self, path: P) -> Result<Fd> {
        let path = path.as_ref();
        let flags = OFlag::O_PATH | OFlag::O_DIRECTORY | OFlag::O_NOFOLLOW | OFlag::O_CLOEXEC;
        nix::fcntl::openat(self.as_raw_fd(), path, flags, Mode::empty())
            .map_err(|source| Error::Open {
                path: path.into(),
                source,
            })
            .map(Fd::Owned)
    }
}

pub fn mount_overlay<P1, P2>(source: &P1, options: Option<&P2>) -> nix::Result<()>
where
    P1: ?Sized + nix::NixPath,
    P2: ?Sized + nix::NixPath,
{
    nix::mount::mount(
        Some("overlay"),
        source,
        Some("overlay"),
        nix::mount::MsFlags::empty(),
        options,
    )
}

pub use nix::mount::umount;
