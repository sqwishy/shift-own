use std::ffi::{OsStr, OsString};
use std::os::unix::ffi::{OsStrExt, OsStringExt};
use std::path::{Path, PathBuf};

use anyhow::{Context, Result};
use signal_hook::{self, iterator::Signals};
use structopt::StructOpt;

mod os;
mod result_ext;

use os::IsErrNo;
use result_ext::{AndAlso, SkipErrIf};

#[derive(Debug, StructOpt)]
#[structopt(author, about)]
pub struct Args {
    #[structopt(short, default_value = "0")]
    shift: shift_own::ArgNumber,
    #[structopt(short, default_value = "0x10000")]
    range: shift_own::ArgNumber,
    #[structopt(
        short = "1",
        long = "oneshot",
        help = "if set, exit after mounting; otherwise, by default, after mounting we wait for SIGINT or SIGTERM before we unmount and quit"
    )]
    oneshot: bool,
    #[structopt(
        help = "source directory (or file maybe?) that will be uid shifted (this path must exist)"
    )]
    source: PathBuf,
    #[structopt(
        help = "path to directory used by for intermediate state (will be created if it does not exist, parents must exist)"
    )]
    intermediate: PathBuf,
    #[structopt(
        help = "output/destination path that will contain the uid shifted source (will be created if it does not exist, parents must exist)"
    )]
    destination: PathBuf,
}

fn main() -> Result<()> {
    let args = Args::from_args();

    let destination = mount_overlay(&args)?;

    // try to umount in case we panic ...
    let umount_later = clean_later(|| {
        let _ = os::umount(destination);
    });

    shift_overlay_owners(destination, &args)?;

    if args.oneshot {
        umount_later.cancel();

        #[cfg(feature = "systemd")]
        sd_notify_ready();

        return Ok(());
    }

    wait_for_exit()?;

    umount_later.cancel();

    os::umount(destination)
        .with_context(|| format!("Failed to unmount {}", destination.display()))?;

    Ok(())
}

fn mount_overlay(args: &Args) -> Result<&PathBuf> {
    let destination = &args.destination;
    let intermediate = &args.intermediate;
    let upper = intermediate.join("upper");
    let working = intermediate.join("working");

    /* create destination */
    os::Dir::cwd()
        .mkdirat(destination)
        .skip_err_if(os::Error::is_file_exists)
        .with_context(|| {
            format!(
                "Failed to create overlay destination directory at {}",
                destination.display()
            )
        })?;

    /* create intermediate directories */
    os::Dir::cwd()
        .mkdirat(intermediate)
        .skip_err_if(os::Error::is_file_exists)
        .and_then(|()| os::Dir::cwd().opendirat(&intermediate).map(os::Dir::from))
        .and_also(|d| d.mkdirat("upper").skip_err_if(os::Error::is_file_exists))
        .and_also(|d| d.mkdirat("working").skip_err_if(os::Error::is_file_exists))
        .with_context(|| {
            format!(
                "Failed to create overlay intermediate directories at {}",
                intermediate.display()
            )
        })?;

    let options = OsString::from_vec(
        vec![
            OsStr::new("lowerdir="),
            args.source.as_os_str(),
            OsStr::new(",upperdir="),
            upper.as_os_str(),
            OsStr::new(",workdir="),
            working.as_os_str(),
            /* metacopy allows chowning the files without copying their
             * contents up from lower layers; very handy */
            OsStr::new(",metacopy=on"),
        ]
        .into_iter()
        .map(|s| s.as_bytes())
        .flatten()
        .cloned()
        .collect::<Vec<u8>>(),
    );

    os::mount_overlay(destination, Some(options.as_os_str())).with_context(|| {
        format!(
            "Failed to mount initial overlay to {} using {}",
            destination.display(),
            options.to_string_lossy(),
        )
    })?;

    println!("Mounted {}", destination.display());

    Ok(destination)
}

fn shift_overlay_owners<P: AsRef<Path>>(destination: P, args: &Args) -> Result<()> {
    let count = shift_own::shift_own_recursive(
        destination.as_ref(),
        &shift_own::ShiftRange {
            shift: *args.shift,
            range: *args.range,
        },
    )
    .try_fold(0usize, |sum, res| res.map(|_| sum + 1))
    .with_context(|| format!("Failed to set ownership in overlay."))?;

    println!(
        "{} files under {} shifted with {} using range {}",
        // this is a bit misleading since not every file we saw was shifted if they
        // already had the correct owner ...
        count,
        destination.as_ref().display(),
        args.shift,
        args.range
    );

    Ok(())
}

fn wait_for_exit() -> Result<()> {
    let signals = Signals::new(&[signal_hook::SIGINT, signal_hook::SIGTERM])
        .context("Could not set up signal handlers for SIGINT and SIGTERM?")?;

    println!("Waiting for SIGINT or SIGTERM ...");

    #[cfg(feature = "systemd")]
    sd_notify_ready();

    match signals.into_iter().next() {
        Some(sig) => println!("Got signal {}; unmounting ...", sig),
        _ => println!("Maybe we got a signal, but I don't know what; unmounting ..."),
    }

    Ok(())
}

#[cfg(feature = "systemd")]
fn sd_notify_ready() {
    use libsystemd_sys::daemon::sd_notify;
    use std::ffi::CString;

    let ready = CString::new("READY=1").expect("CString::new failed");
    let p = ready.as_ptr();
    unsafe { sd_notify(0, p) };
}

struct Cleanup<F: FnOnce()>(Option<F>);

impl<F> Drop for Cleanup<F>
where
    F: FnOnce(),
{
    fn drop(&mut self) {
        if let Some(f) = self.0.take() {
            (f)()
        }
    }
}

impl<F> Cleanup<F>
where
    F: FnOnce(),
{
    fn cancel(mut self) {
        self.0.take();
    }
}

fn clean_later<F: FnOnce()>(f: F) -> Cleanup<F> {
    Cleanup(Some(f))
}
