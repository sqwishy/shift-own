pub trait AndAlso: Sized {
    type T;
    type E;

    fn and_also<F>(self, func: F) -> Result<Self::T, Self::E>
    where
        F: FnOnce(&Self::T) -> Result<(), Self::E>;
}

impl<T, E> AndAlso for Result<T, E> {
    type T = T;
    type E = E;

    fn and_also<F>(self, func: F) -> Result<Self::T, Self::E>
    where
        F: FnOnce(&Self::T) -> Result<(), Self::E>,
    {
        self.and_then(|t| func(&t).map(|()| t))
    }
}

pub trait SkipErrIf: Sized {
    type E;

    fn skip_err_if<F>(self, func: F) -> Result<(), Self::E>
    where
        F: FnOnce(&Self::E) -> bool;
}

impl<E> SkipErrIf for Result<(), E> {
    type E = E;

    fn skip_err_if<F>(self, func: F) -> Result<(), Self::E>
    where
        F: FnOnce(&Self::E) -> bool,
    {
        self.or_else(|e| if func(&e) { Ok(()) } else { Err(e) })
    }
}
