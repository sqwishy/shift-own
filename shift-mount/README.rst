Create an overlay mount of some *source* at some *destination*. File ownership is
modified for the overlay given some *range* and *shift* using: :code:`owner % range
+ shift`.

(See the *shift-own* project for why you might want to do this.)

The overlay is created by passing "metacopy=on" to mount. This way, when file ownership
is modified for the destination mount point, the contents of files are not copied up
into the overlay.

This feature uses some fancy stuff, like extended file attributes, that must be
supported on the filesystem that the overlay's intermediate (workdir and upperdir)
directories use. If you place these on a tmpfs, for example, you may get EOPNOTSUPP
errors.

Also: "Do not use metacopy=on with untrusted upper/lower directories." See the kernel's
documentation_ on Linux overlayfs.

.. _documentation: https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/filesystems/overlayfs.rst
