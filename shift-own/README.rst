Modify file ownership using a *shift* and *range* value.

For each user and group ids, derive a new id using: :code:`uid % range + shift`

You might want to do this if you have your user and group id range chunked out into 0x10000 sized
reservations for uid mapped Linux namespaces or containers something.

.. code::

  0             root on host
  1000          user on host
  0x10000       root on namespace0
  0x10000+1000  user on namespace0
  0x20000       root on namespace1
  0x20000+1000  user on namespace1
  ...

Suppose we want to move a file from one namespace to another and maintain the ownership
relative to the namespace the file is in. We can map the ownership using the above
expression using the beginning of the destination namespace's reservation (0x10000,
0x20000, ...) for the *shift* value and the reservation size (0x10000) for the
*range*.

This rust crate is available as a library and also has a binary target called
*shift-own*.

Tests probably require root privileges.

See also the *shift-mount* program that uses this library with an overlayfs mount to
provide a view of a directory with source file ownership mapped using the above
strategy.
