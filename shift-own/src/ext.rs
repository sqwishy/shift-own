/// A bunch of *Ext traits for other traits.
///
/// - AsRawFd::chown
/// - AsRef<Path>::to_cstring
/// - AsRef<Path>::touch
/// - AsRef<Path>::lchown
/// - MetadataExt::owner
use std::ffi::CString;
use std::os::linux::fs::MetadataExt;
use std::os::unix::{ffi::OsStrExt, io::AsRawFd};
use std::path::Path;
use std::{fs, io};

use crate::Owner;

pub trait FdExt: AsRawFd {
    fn chown(&self, uid: u32, gid: u32) -> io::Result<&Self> {
        let fd = self.as_raw_fd();
        if unsafe { libc::fchown(fd, uid, gid) } < 0 {
            return Err(io::Error::last_os_error());
        }
        return Ok(self);
    }
}

impl<F: AsRawFd> FdExt for F {}

pub trait PathExt: AsRef<Path> {
    fn to_cstring(&self) -> CString {
        let path = self.as_ref().as_os_str().as_bytes();
        let mut buf: Vec<u8> = Vec::with_capacity(path.len() + 1);
        buf.extend(path);
        unsafe { CString::from_vec_unchecked(buf) }
    }

    fn touch(&self) -> io::Result<fs::File> {
        use std::fs::OpenOptions;
        OpenOptions::new().write(true).create_new(true).open(self)
    }

    fn mkdir(&self) -> io::Result<&Self> {
        fs::create_dir(self).map(|()| self)
    }

    fn symlink<P: AsRef<Path>>(&self, path: P) -> io::Result<&Self> {
        let link = self.to_cstring();
        let target = path.to_cstring();
        if unsafe { libc::symlink(target.as_ptr(), link.as_ptr()) } < 0 {
            return Err(io::Error::last_os_error());
        }
        return Ok(self);
    }

    fn lchown(&self, uid: u32, gid: u32) -> io::Result<&Self> {
        let path = self.to_cstring();
        if unsafe { libc::lchown(path.as_ptr(), uid, gid) } < 0 {
            return Err(io::Error::last_os_error());
        }
        return Ok(self);
    }
}

impl<P: AsRef<Path>> PathExt for P {}

pub trait MetadataExtExt: MetadataExt {
    fn owner(&self) -> Owner {
        Owner {
            uid: self.st_uid(),
            gid: self.st_gid(),
        }
    }
}

impl<M: MetadataExt> MetadataExtExt for M {}
