use crate::ext::{MetadataExtExt, PathExt};
use crate::{shift_own, Owner, ShiftRange, ShiftTarget};

type Error = Box<dyn std::error::Error>;

mod temp {
    use std::ffi::{CString, OsString};
    use std::os::unix::ffi::OsStringExt;
    use std::path::{Path, PathBuf};
    use std::{fs, io};

    use crate::ext::PathExt;

    pub struct Directory(PathBuf);

    impl Directory {
        pub fn path(&self) -> &Path {
            self.0.as_path()
        }
    }

    impl Drop for Directory {
        fn drop(&mut self) {
            if let Err(e) = fs::remove_dir_all(self.path()) {
                eprintln!("error dropping temporary {:?}: {}", self.path(), e);
            }
        }
    }

    pub fn dir<P: AsRef<Path>>(path: P) -> io::Result<Directory> {
        let template = path.to_cstring();
        let template = unsafe { libc::mkdtemp(template.into_raw()) };
        if template.is_null() {
            return Err(io::Error::last_os_error());
        }

        let buf = unsafe { CString::from_raw(template) }.into_bytes();
        Ok(Directory(PathBuf::from(OsString::from_vec(buf))))
    }
}

#[test]
fn test_path() -> Result<(), Error> {
    let tmp = temp::dir("/tmp/shown-XXXXXX")?;

    tmp.path().lchown(1000, 1000)?;

    let shift = ShiftRange {
        shift: 0x10000,
        range: 0x10000,
    };

    shift_own(tmp.path(), &shift)?;

    assert_eq!(
        tmp.path().metadata()?.owner(),
        Owner {
            uid: 0x10000 + 1000,
            gid: 0x10000 + 1000
        }
    );

    Ok(())
}

#[test]
fn test_dry_run() -> Result<(), Error> {
    let tmp = temp::dir("/tmp/shown-XXXXXX")?;

    tmp.path().lchown(1000, 1000)?;

    shift_own(
        tmp.path().dry_run(),
        &ShiftRange {
            shift: 0x20000,
            ..ShiftRange::default()
        },
    )?;

    assert_eq!(
        tmp.path().metadata()?.owner(),
        Owner {
            uid: 1000,
            gid: 1000
        }
    );

    Ok(())
}

#[test]
#[cfg(feature = "walk")]
fn test_walkdir() -> Result<(), Error> {
    use crate::ext::FdExt;
    use crate::ext::MetadataExtExt;

    let tmp_dir = temp::dir("/tmp/shown-XXXXXX")?;
    let tmp = tmp_dir.path();

    tmp.join("0").mkdir()?.lchown(0, 0)?;
    tmp.join("0/0").mkdir()?.lchown(0, 0)?;
    tmp.join("0/0/a").touch()?.chown(0, 0)?;
    tmp.join("0/1").mkdir()?.lchown(1000, 1000)?;
    tmp.join("0/a").touch()?.chown(1000, 1000)?;
    tmp.join("0/b").symlink("..")?.lchown(1000, 1000)?;

    let shift = ShiftRange {
        shift: 0x30000,
        range: 0x10000,
    };

    for entry in walkdir::WalkDir::new(tmp).into_iter() {
        let mut entry = entry?;
        eprintln!("{}", entry.path().display());
        shift_own(crate::WalkEntry(&mut entry), &shift)?;
    }

    struct Expect {
        path: &'static str,
        uid: u32,
        gid: u32,
    }

    let expects = vec![
        Expect {
            path: "0",
            uid: 0x30000,
            gid: 0x30000,
        },
        Expect {
            path: "0/0",
            uid: 0x30000,
            gid: 0x30000,
        },
        Expect {
            path: "0/0/a",
            uid: 0x30000,
            gid: 0x30000,
        },
        Expect {
            path: "0/1",
            uid: 0x30000 + 1000,
            gid: 0x30000 + 1000,
        },
        Expect {
            path: "0/a",
            uid: 0x30000 + 1000,
            gid: 0x30000 + 1000,
        },
        Expect {
            path: "0/b",
            uid: 0x30000 + 1000,
            gid: 0x30000 + 1000,
        },
    ];

    for Expect { path, uid, gid } in expects.into_iter() {
        let got = tmp.join(path).metadata()?.owner();
        assert_eq!(got, Owner { uid, gid }, "{}", path);
    }

    Ok(())
}
