use std::{
    fmt,
    fs::Metadata,
    io,
    path::{Path, PathBuf},
};

#[cfg(feature = "walk")]
use std::borrow::BorrowMut;

mod ext;
#[cfg(test)]
mod tests;

use ext::{MetadataExtExt, PathExt};

#[derive(Debug, thiserror::Error)]
pub enum Error<T, M>
where
    T: fmt::Debug + std::error::Error + 'static,
    M: fmt::Debug + std::error::Error + 'static,
{
    #[error("Could not read metadata for {}", .path.display())]
    NoMetadata { path: PathBuf, source: T },
    #[error("Mapping ownership of {} failed for {}", .owner, .path.display())]
    FailedMap {
        path: PathBuf,
        owner: Owner,
        source: M,
    },
    #[error("Failed to chown {} to {}", .path.display(), .owner)]
    FailedChown {
        path: PathBuf,
        owner: Owner,
        source: T,
    },
    #[cfg(feature = "walk")]
    #[error("Failed walk")]
    Walk {
        #[from]
        source: walkdir::Error,
    },
}

/// Hexadecimal/0x parsing for --range and --shift.
#[derive(Debug)]
pub struct ArgNumber(u32);

impl std::str::FromStr for ArgNumber {
    type Err = std::num::ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with("0x") {
            u32::from_str_radix(&s[2..], 16).map(Self)
        } else {
            u32::from_str(s).map(Self)
        }
    }
}

impl fmt::Display for ArgNumber {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "0x{:x}", self.0)
    }
}

impl std::ops::Deref for ArgNumber {
    type Target = u32;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Owner {
    pub uid: u32,
    pub gid: u32,
}

impl fmt::Display for Owner {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}:{}", self.gid, self.uid)
    }
}

/// Describes a way to map file owners, for use in [shift_own()].
pub trait OwnerMapping {
    type Error: std::error::Error;

    fn map_owner(&self, o: Owner) -> Result<Owner, Self::Error>;
}

/// An [OwnerMapping] for `uid % range + shift` that can be passed to [shift_own()].
///
/// The ShiftRange::default() has zero shift and a range of 0x10000.
#[derive(Debug, Clone, Copy)]
pub struct ShiftRange {
    pub shift: u32,
    pub range: u32,
}

#[derive(Debug, thiserror::Error)]
#[error("mapping would exceed u32 max value")]
pub struct OutOfRange;

impl Default for ShiftRange {
    fn default() -> Self {
        Self {
            shift: 0,
            range: 0x10000,
        }
    }
}

impl ShiftRange {
    fn map_uid(&self, mut uid: u32) -> Option<u32> {
        if self.range > 0 {
            uid %= self.range;
        }

        uid.checked_add(self.shift)
    }
}

impl OwnerMapping for ShiftRange {
    type Error = OutOfRange;

    fn map_owner(&self, o: Owner) -> Result<Owner, Self::Error> {
        let uid = self.map_uid(o.uid).ok_or(OutOfRange)?;
        let gid = self.map_uid(o.gid).ok_or(OutOfRange)?;
        Ok(Owner { uid, gid })
    }
}

pub trait ShiftTarget {
    type Error: std::error::Error;

    fn path(&self) -> &Path;
    fn metadata(&self) -> Result<Metadata, Self::Error>;
    fn chown(&mut self, o: Owner) -> Result<(), Self::Error>;

    /// Move this in a [DryRun] that turns [ShiftTarget::chown()] into a no-op.
    fn dry_run(self) -> DryRun<Self>
    where
        Self: Sized,
    {
        DryRun(self)
    }
}

/// Implements [ShiftTarget] for paths to filesystem objects, does not follow symlinks.
impl<P: AsRef<Path>> ShiftTarget for P {
    type Error = io::Error;

    fn path(&self) -> &Path {
        self.as_ref()
    }

    fn metadata(&self) -> io::Result<Metadata> {
        self.path().symlink_metadata()
    }

    fn chown(&mut self, o: Owner) -> io::Result<()> {
        self.lchown(o.uid, o.gid).map(|_| ())
    }
}

/// Wraps any [ShiftTarget] implementation that turns [ShiftTarget::chown()] into a no-op.
#[derive(Debug)]
pub struct DryRun<T>(pub T);

impl<T> ShiftTarget for DryRun<T>
where
    T: ShiftTarget,
{
    type Error = T::Error;

    fn path(&self) -> &Path {
        self.0.path()
    }

    fn metadata(&self) -> Result<Metadata, Self::Error> {
        self.0.metadata()
    }

    fn chown(&mut self, _: Owner) -> Result<(), Self::Error> {
        Ok(())
    }
}

#[cfg(feature = "walk")]
/// A newtype around [walkdir::DirEntry] that implements [ShiftTarget].
///
/// Requires the "walk" feature, which is enabled by default.
///
/// This is needed because rust won't let me implement a trait for AsRef<Path> and
/// [walkdir::DirEntry]. Because one day, the latter might implement the former. And then all hell
/// would break loose.
///
/// I actually don't know what to do about this. I've asked around but nobody seems to have any
/// answer.  Mozilla is shutting down their IRC server so that place is dead. Questions in the
/// channels on discord aren't replied to unless a) you show code b) the answer to your question is
/// either "you can't do that because it's undefined behaviour" or "yeah that's a known bug in
/// rust".
#[derive(Debug)]
pub struct WalkEntry<E: BorrowMut<walkdir::DirEntry>>(pub E);

#[cfg(feature = "walk")]
impl<E: BorrowMut<walkdir::DirEntry>> ShiftTarget for WalkEntry<E> {
    type Error = io::Error;

    fn path(&self) -> &Path {
        self.0.borrow().path()
    }

    fn metadata(&self) -> io::Result<Metadata> {
        Ok(self.0.borrow().metadata()?)
    }

    fn chown(&mut self, o: Owner) -> io::Result<()> {
        self.path().lchown(o.uid, o.gid).map(|_| ())
    }
}

/// Read the ownership of the file, use the [OwnerMapping] to derive a new [Owner], then modify the
/// file's owner if the new owner is different.
///
/// On success, returns the old and new owner (even if they're the same).
pub fn shift_own<T, M>(
    mut t: T,
    map: &M,
) -> Result<(Owner, Owner), Error<<T as ShiftTarget>::Error, <M as OwnerMapping>::Error>>
where
    T: ShiftTarget,
    M: OwnerMapping,
{
    let old_owner = t
        .metadata()
        .map_err(|source| Error::NoMetadata {
            path: t.path().into(),
            source,
        })?
        .owner();

    let new_owner = map
        .map_owner(old_owner)
        .map_err(|source| Error::FailedMap {
            path: t.path().into(),
            owner: old_owner,
            source,
        })?;

    if new_owner != old_owner {
        t.chown(new_owner).map_err(|source| Error::FailedChown {
            path: t.path().into(),
            owner: new_owner,
            source,
        })?;
    }

    Ok((old_owner, new_owner))
}

/// Creates a [walkdir::WalkDir] and calls [shift_own()] on each yielded entry.
///
/// By default this shouldn't follow symbolic links.
///
/// Requires the "walk" feature, which is enabled by default.
#[cfg(feature = "walk")]
pub fn shift_own_recursive<'m, P, M>(
    path: P,
    map: &'m M,
) -> impl Iterator<Item = Result<(Owner, Owner), Error<io::Error, <M as OwnerMapping>::Error>>> + 'm
where
    P: AsRef<Path>,
    M: OwnerMapping,
    <M as OwnerMapping>::Error: 'static,
{
    let path = path.as_ref();
    let walk = walkdir::WalkDir::new(path);
    walk.into_iter().map(move |entry| {
        let entry = entry?;
        shift_own(WalkEntry(entry), map)
    })
}
