use std::path::{Component, Path, PathBuf};

use clap::value_t_or_exit;

use shift_own::{shift_own, ArgNumber, Owner, ShiftRange, ShiftTarget, WalkEntry};

const NAME: &'static str = env!("CARGO_PKG_NAME");
const VERSION: &'static str = env!("CARGO_PKG_VERSION");
const AUTHORS: &'static str = env!("CARGO_PKG_AUTHORS");
const DESCRIPTION: &'static str = env!("CARGO_PKG_DESCRIPTION");
const BIG_HELP: &'static str = r#"Recursively walk paths and set file user and group: `uid % range + shift`

Some notes!
- Stops if an error is encountered.
- Does not follow symlinks.
"#;

use anyhow::Result;

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Verbosity {
    // Stfu,
    Chillin,
    Changes,
    Everything,
}

#[derive(Debug)]
pub struct Args {
    pub preserve_root: bool,
    pub dry_run: bool,
    pub shift: ShiftRange,
    pub verbosity: Verbosity,
}

fn main() -> Result<()> {
    let clap = clap::App::new(NAME)
        .version(VERSION)
        .author(AUTHORS)
        .about(DESCRIPTION)
        .after_help(BIG_HELP)
        .arg_from_usage("-n, --dry-run 'Do not attempt to chown'")
        .arg_from_usage("-v... 'Use -v to print modifications or -vv to print all visited files'")
        .arg_from_usage("--no-preserve-root 'Disables default behaviour of skipping over '/' paths given on the command line'")
        .arg(
            clap::Arg::from_usage(
                "-s, --shift=SHIFT 'Increase the uid/gid by a positive number'"
            ).default_value("0")
        )
        .arg(
            clap::Arg::from_usage(
                "-r, --range=RANGE 'Modulo the uid/gid by a positive number (ignored if zero)'"
            ).default_value("0x10000")
        )
        .arg(clap::Arg::from_usage("<paths>... 'Paths to shift ownership of'"))
        ;

    let matches = clap.get_matches();

    let paths = matches
        .values_of_os("paths")
        .unwrap()
        .map(|v| PathBuf::from(v))
        .collect();

    let args = Args {
        preserve_root: !matches.is_present("no-preserve-root"),
        dry_run: matches.is_present("dry-run"),
        shift: ShiftRange {
            shift: *value_t_or_exit!(matches, "shift", ArgNumber),
            range: *value_t_or_exit!(matches, "range", ArgNumber),
        },
        verbosity: match matches.occurrences_of("v") {
            0 => Verbosity::Chillin,
            1 => Verbosity::Changes,
            _ => Verbosity::Everything,
        },
    };

    return shift_paths(paths, &args);
}

fn shift_paths<P: AsRef<Path>>(paths: Vec<P>, args: &Args) -> Result<()> {
    for path in paths {
        shift_path(path, args)?
    }

    Ok(())
}

fn shift_path<P: AsRef<Path>>(path: P, args: &Args) -> Result<()> {
    let path = path.as_ref();

    if args.preserve_root {
        let mut comp = path.components();
        if (comp.next(), comp.next()) == (Some(Component::RootDir), None) {
            eprintln!(
                "Ignoring '/' path. Run with --no-preserve-root if you can be a serious person."
            );
            return Ok(());
        }
    }

    let walk = walkdir::WalkDir::new(path);

    for entry in walk {
        let mut entry = entry?;

        let walk_entry = WalkEntry(&mut entry);
        let (old, new) = if args.dry_run {
            shift_own(walk_entry.dry_run(), &args.shift)?
        } else {
            shift_own(walk_entry, &args.shift)?
        };

        match args.verbosity {
            Verbosity::Everything => print_change(entry.path(), old, new),
            Verbosity::Changes if old != new => print_change(entry.path(), old, new),
            _ => (),
        }
    }

    Ok(())
}

fn print_change<P: AsRef<Path>>(path: P, old: Owner, new: Owner) {
    let sym = if old == new { "==" } else { "->" };
    println!("{} {} {} .. {}", old, sym, new, path.as_ref().display(),)
}
